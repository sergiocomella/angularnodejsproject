import { NgModule } from "@angular/core";

import {
  MatMenuModule,
  MatInputModule,
  MatCardModule,
  MatExpansionModule,
  MatToolbarModule,
  MatButtonModule,
  MatDialogModule,
  MatPaginatorModule,
  MatTooltipModule,
  MatIconModule,
  MatProgressSpinnerModule
} from "@angular/material";

@NgModule({
  imports: [
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule
  ],
  exports: [
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule
  ]
})
export class AngularMaterialModule {}
