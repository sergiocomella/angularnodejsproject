let router = require("express").Router();
let controller = require("../controller/postController");
const checkAuth = require("../middleware/check-auth");
const extractFile = require("../middleware/file");

router
  .route("/")
  .post(
    checkAuth,
   extractFile,
    controller.addPost
  )
  .get(controller.getAllPost);

router.route("/delete/:id").delete(checkAuth, controller.deletePost);

router
  .route("/:id")
  .put(
    checkAuth,
    extractFile,
    controller.updatePost
  )
  .get(controller.getPost);


module.exports = router;
