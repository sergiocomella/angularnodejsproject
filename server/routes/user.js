let router = require("express").Router();
let controller = require("../controller/userController");

router
  .route("/signup")
  .post(controller.signupNewUser);

router
  .route("/login")
  .post(controller.loginUser);


module.exports = router;