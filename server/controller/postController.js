const Post = require("../models/postModel");

exports.addPost = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  let newPost = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images/" + req.file.filename,
    creator: req.userData.userId
  });
  newPost.save().then((createdPost, err) => {
    if (err) return next(err);
    res.status(201).json({
      message: "Post Created",
      post: {
        ...createdPost,
        id: createdPost._id
      }
    });
  }).catch(error => {
      res.status(500).json({
        message: 'Creazione del post fallita.'
      })
    });
};

exports.getAllPost = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fetchedPosts;
  if (pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  postQuery
    .then((posts, error) => {
      if (error) return next(error);
      fetchedPosts = posts;
      return Post.countDocuments();
    })
    .then(count => {
      res.status(200).json({
        message: "Post recuperati",
        posts: fetchedPosts,
        maxPosts: count
      });
    }).catch(error => {
    res.status(500).json({
      message: 'Errore nel recupero dei post.'
    })
  });
};

exports.getPost = (req, res, next) => {
  Post.findById(req.params.id).then((post, error) => {
    if (error) return next(error);

    if (!!post) {
      res.status(200).json(post);
    } else {
      res.status(404).json("Post not found");
    }
  });
};

exports.deletePost = (req, res, next) => {
  Post.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then(
    (result, error) => {
      if (error) return next(error);
      if (result.n > 0) {
        res.status(200).json({
          message: "Post deleted",
          details: result
        });
      } else {
        res.status(401).json({
          message: "Non sei autorizzato per quest'operazione."
        });
      }
    }
  );
};

exports.updatePost = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const updatedPost = new Post({
    _id: req.params.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  });
  Post.updateOne(
    { _id: req.params.id, creator: req.userData.userId },
    updatedPost
  ).then((result, error) => {
    if (error) return next(error);
    console.log({result: result});
    if (result.n > 0) {
      res.status(200).json({
        message: "Post updated",
        details: result
      });
    } else {
      res.status(401).json({
        message: "Non sei autorizzato per quest'operazione."
      });
    }
  }).catch(error => {
    res.status(500).json({
      message: 'Aggiornamento del post fallito.'
    })
  });
};
